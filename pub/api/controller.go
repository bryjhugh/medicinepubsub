package api

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

/*
	Structures, Redis, Context
*/
type PillRequest struct {
	BatchId    uuid.UUID `json:"batchId"`
	BatchCount int       `json:"batchCount"`
}

type FulfilledPill struct {
	BatchId    uuid.UUID `json:"batchId"`
	Id         uuid.UUID `json:"id"`
	BatchCount int       `json:"batchCount"`
}

var redisClient = redis.NewClient(&redis.Options{
	Addr: "redis:6379",
})

var ctx = context.Background()

/*
	---- Additional Function information in main.go ----
*/

//Endpoint handler for creating a custom batch
//largely used in testing
func CreateCustomBatch(c *fiber.Ctx) error {
	//Parse Request
	totalRequested, err := c.ParamsInt("totalRequested")
	if err != nil {
		fmt.Println(err)
		fmt.Println("Input Invalid. Using default batch number of 15")
		totalRequested = 15
	}
	fmt.Printf("Recieved request for %d pills\n", totalRequested)
	CreateBatch(totalRequested)
	return c.SendStatus(200)
}

// Creates Batch
// Easily usable by internal functions
func CreateBatch(totalRequested int) {

	//Build batch request to send
	batchId := uuid.New()
	pillRequest := &PillRequest{BatchId: batchId, BatchCount: totalRequested}

	//Create payload
	payload, err := json.Marshal(pillRequest)
	if err != nil {
		fmt.Println("Error Creating Batch Request")
	}

	//Send to Redis queue (left push)
	if err := redisClient.LPush(ctx, "request-queue", payload).Err(); err != nil {
		fmt.Println(err)
		fmt.Println("Error Pushing Batch Request")
	}
	fmt.Printf("Created batch request and sent with id: %+s\n", pillRequest.BatchId)
}

//Recieves the created amount of pills
func DeliverBatch(c *fiber.Ctx) error {
	//Get Response
	deliveredBatch := new([]string)
	if err := json.Unmarshal([]byte(c.Body()), deliveredBatch); err != nil {
		fmt.Println(err)
		fmt.Println("error unmarshaling json")
	}

	//Pull out batchId from response
	data := new(FulfilledPill)
	if len((*deliveredBatch)) > 0 {
		if err := json.Unmarshal([]byte((*deliveredBatch)[0]), &data); err != nil {
			fmt.Println(err)
			fmt.Println("error getting batchId")
		}
	} else {
		fmt.Printf("Received malformed payload\n")
		return c.SendStatus(400)
	}
	fmt.Printf("Successfully Delivered batch: %+s\n", data.BatchId.String())

	return c.SendStatus(200)

	//Given more purpose and time data can be managed and manipulated
	//Not focusing on this as much to spend more time on k8 scaling
}

func BeginDemo() {
	for {
		reqNum := rand.Intn(30)
		CreateBatch(reqNum)
		time.Sleep(2 * time.Second)
	}
}

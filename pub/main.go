// @/pub/main.go
package main

import (
	"bh/redisPublisher/api"
	"os"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	/* ---
	Method: createbatch
	Purpose: Creates a batch to be delivered via redis to the consumers

	Additonal: This function is one which can be called over and over again to generate
	batches of medicine. It would also be able to pass through unique identifiers to relate
	a batch to a request should that be desired
	   --- */
	app.Get("/createBatch/:totalRequested", api.CreateCustomBatch)

	/* ---
	Method: deliverBatch
	Purpose: Receives a batch of completed pills grouped by BatchID

	Additonal: This function exists to show how a delivery service may send
	data to external endpoints using restful endpoints. While it resides within
	the publisher it could just as easily reside within any client that wants to
	consume the data
	   --- */
	app.Post("/deliverBatch", api.DeliverBatch)

	if os.Getenv("ENV_NAME") != "local" {
		go api.BeginDemo()
	}
	app.Listen(":3000")

}

// @/sub/main.go
package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
)

type PillRequest struct {
	BatchId    uuid.UUID `json:"batchId"`
	BatchCount int       `json:"batchCount"`
}

type FulfilledPill struct {
	BatchId    uuid.UUID `json:"batchId,string"`
	Id         uuid.UUID `json:"id,string"`
	BatchCount int       `json:"batchCount,string"`
}

var ctx = context.Background()

var redisClient = redis.NewClient(&redis.Options{
	Addr: "redis:6379",
})

func main() {
	// dont use the following! Published to ALL subscribers
	//subscriber := redisClient.Subscribe(ctx, "request-queue")
	deliveryQueue()
}

//Monitors the delivery queue
func deliveryQueue() {
	pillRequest := PillRequest{}
	for {
		//Pop request and get data
		queueList, err := redisClient.BLPop(ctx, 0, "fulfillment-queue").Result()
		if err != nil {
			fmt.Println("error receivingMessage")
		}
		pillRequestList := queueList[1]

		if err := json.Unmarshal([]byte(pillRequestList), &pillRequest); err != nil {
			fmt.Println("error unmarshaling json")
		}
		fmt.Printf("Delivery Requested for: %s\n", pillRequest.BatchId.String())

		//Go get the fulfilled items and deliver
		completeDelivery(pillRequest)
	}
}

//gets fulfilled items, sends via http to publisher
func completeDelivery(pillRequest PillRequest) {

	queueList, err := redisClient.LRange(ctx, pillRequest.BatchId.String(), 0, -1).Result()
	if err != nil {
		fmt.Println("error receivingMessage")
	}

	payload, err := json.Marshal(queueList)
	if err != nil {
		fmt.Println(err)
	}

	reader := bytes.NewReader(payload)

	//Send to Publisher
	var (
		resp    *http.Response
		retries int = 3
	)
	for retries > 0 {
		resp, err = http.Post("http://publish-service:3000/deliverBatch", "application/json", reader)
		if err != nil {
			retries -= 1
			fmt.Println("Failure sending batch " + pillRequest.BatchId.String())
			if retries == 0 {
				fmt.Println("Giving up on " + pillRequest.BatchId.String())
			}
			fmt.Println(err)
			time.Sleep(1 * time.Second)
		} else {
			break
		}
	}
	fmt.Println("Delivery Complete with status code: ", resp.StatusCode)
	defer resp.Body.Close()
	redisClient.Del(ctx, pillRequest.BatchId.String())
}

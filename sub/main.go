// @/sub/main.go
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
)

type PillRequest struct {
	BatchId    uuid.UUID `json:"batchId"`
	BatchCount int       `json:"batchCount"`
}

type FulfilledPill struct {
	BatchId    uuid.UUID `json:"batchId"`
	Id         uuid.UUID `json:"id"`
	BatchCount int       `json:"batchCount"`
}

var ctx = context.Background()

var redisClient = redis.NewClient(&redis.Options{
	Addr: "redis:6379",
})

func main() {
	//do not ues the following. Publishes to all subscribers.
	//subscriber := redisClient.Subscribe(ctx, "request-queue")
	pillRequest := PillRequest{}

	for {
		queueList, err := redisClient.BLPop(ctx, 0, "request-queue").Result()
		if err != nil {
			fmt.Println("error receivingMessage")
		}
		pillRequestList := queueList[1]

		if err := json.Unmarshal([]byte(pillRequestList), &pillRequest); err != nil {
			fmt.Println("error unmarshaling json")
		}
		fmt.Printf("Subscriber recieved batch requested %+s for %d pills\n", pillRequest.BatchId.String(), pillRequest.BatchCount)

		//Loop to fulfill 1 pill at a time which also sends it to delivery queue
		for currentPillRequest := 1; currentPillRequest <= pillRequest.BatchCount; currentPillRequest++ {
			batchId := uuid.New()
			unsentPill := FulfilledPill{BatchId: pillRequest.BatchId, Id: batchId, BatchCount: pillRequest.BatchCount}
			fulfillSinglePill(unsentPill)
		}

		sendCompletion(pillRequest)

	}
}

func fulfillSinglePill(singlePill FulfilledPill) {
	fmt.Println("Fulfilling ", singlePill.Id)
	time.Sleep(2 * time.Second)
	payload, err := json.Marshal(singlePill)
	if err != nil {
		fmt.Println(err)
	}

	//push to queue using UUID as name
	if err := redisClient.LPush(ctx, singlePill.BatchId.String(), payload).Err(); err != nil {
		fmt.Println(err)
	}
}

func sendCompletion(pillRequest PillRequest) {
	fmt.Printf("Sending Completion:  %+s\n", pillRequest.BatchId.String())
	payload, err := json.Marshal(pillRequest)
	if err != nil {
		fmt.Println(err)
	}

	//Notify delivery service that request is ready to be delivered
	if err := redisClient.LPush(ctx, "fulfillment-queue", payload).Err(); err != nil {
		fmt.Println(err)
	}
}

# Publisher Subscriber Example with Go, Redis, and Keda

Welcome to a brief demo of a publisher subscriber you'll find basic startup steps. Please note that Go code will be very basic as I am using this project to learn it. 

### Prerequisites (non exhaustive)
Local Development
* Docker, Docker-compose,DockerDesktop 
* The ability to compile GO and use Makefiles

Local Cluster Deployment 
* Minikube
* Probably others

#### Running options
There are 2 ways to run the demo.
* Locally on docker
* On a Kubernetese cluster

These 2 ways of running the demo also affect the behavior. Running locally using
docker-compose will not automatically create batches to be handled, instead allowing
a developer to create their own batches using an application like Postman. The demo
is setup to allow a person to create batches via browser GET as well.

## Running via Docker-Compose
#### Setup
1) Clone Repository
2) Start Docker Desktop (for monitoring)
3) Navigate using a terminal (git-bash for me) to parent directory with docker-compose
#### Running
1) Use command "make work"
#### Monitor pods
Producer Pod: will show that a web application has started\
Subscriber Pod: will just run and not display anything for the moment\
Delivery Pod: will just run and not display anything for the moment\
Redis Pod: will show normal Redis startup (not a custom image)
#### Test
1)Use Postman or browser to send a request to `localhost:3000/createBatch/5` to create a batch with 5 items\
2)Monitor logs as messages flow from Producer -> Subscriber -> Delivery -> Producer

## Running via Minikube
*Note: K8 is currently setup to use gitlab registry image*
#### Setup
1) Clone Repository
2) Start Minikube
3) Start Minikube Dashboard (`minikube dashboard` in a seperate terminal)
#### Running
1) Navigate to main folder and run `kubectl apply -f k8/` and wait for successful deploy
2) Using Dashboard monitor pods and services
##### Optional
* Run `minikube service publish-service` in a separate terminal to expose web app for API testeing

#### Future Pipeline 

The Pipeline will have these general steps:
1) Linting
2) Code Building
3) Code Testing (and scans)
4) Container Building
5) Container testing (security scans)
6) Deployment script linting
7) Automated deployment to lower environments
8) Semantic Versioning, Release Creation


## Architecture!!

#### Work Flow (Human readable)
(Publisher) - A Publisher will create a batch of medicine to be fulfilled, like a prescription. We will give it an ID and a number requested\
(Subscriber)- like a pharmacy worker, the subscriber will pick up the request and fulfill it. This takes ~2 seconds per item requested in the batch\
(Delivery) - Will be notified that the pharmacy worker has completed fulfillment and put the order in a box. Delivery will pickup the box, package it, and send it to the publisher. 
#### Work Flow (Code Structure)
**Publisher**\
The publisher will consist of an API and a looped process which continually creates batches at a constant rate. The looped process exists to create constant load to test the autoscaling when deployed in a kubernetes cluster. The API exists to test individual batches on local docker image without overloading non-scaled images. After a batch has been created it will be put onto a Redis list. *Note: a Redis Publisher/Subscriber model publishes every message to all subscribers! Using those functions would require complicated solutions or they would end up creating duplicate batches. So we will not be using the default Redis pub/sub functions.*

**Subscriber**\
The Subscriber will be an auto-scale capable pod which pulls a request from a request-queue and fulfills it. It does this by iterating the number of requested times, and placing each individual item requested into a seperate delivery list using its UUID as the name of the list. After it has completed the entire batch, it will place a final message on a delivery-queue which will inform the delivery service that an order has been completed.

**Delivery**\
The delivery service will monitor the delivery-queue waiting for a single message which contains the original batchId and items requested number. It will then use the UUID (batchId) as the name of the list to pull from on Redis. It will pull all messages, wrap them into one item, and deliver them via HTTP Post to the Publisher. This is used to represent delivery to an external customer rather than an internal one. Delivering in such a fashion could be done to any accepting endpoint.

**Subscriber Autoscale**\
The Autoscaler will use the messages in the request-queue to determine if it needs to spin up or spin down pods. When the number of batch requests is stable in the queue is stable, then the correct number of pods will be in use. 




